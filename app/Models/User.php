<?php
/**
 * Created by PhpStorm.
 * User: Brieuc
 * Date: 26/10/2018
 * Time: 13:55
 */

namespace App\Model;

class User extends Model
{
    protected $date;
    protected $first_name;
    protected $last_name;
    protected $email;
    protected $lastlogin;

    public function __construct($date, $first_name, $last_name, $email)
    {
        $this->date = $date;
        $this->$first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
    }

    public function getFullName() {
        return $this->first_name . $this->last_name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }
}