<?php

require __DIR__ . '/../vendor/autoload.php';

//use App\Models\User;
//use App\Models\Model;

$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

$host = getenv('DATABASE_SERVER');
$dbname = getenv('DATABASE_NAME');
$user = getenv('DATABASE_USER');
$password = getenv('DATABASE_PASSWORD');

use Carbon\Carbon;


class Model {

    public function changeCreatedDate($date) {
        $newdate = Carbon::createFromDate($date)->locale('fr_FR');
        return $newdate->isoFormat('DD/MM/YYYY');
    }

    public function changeDate($date) {
        $newdate = Carbon::createFromDate($date);
        return $newdate->diffForHumans();
    }
}


class User extends Model
{
    protected $date;
    protected $first_name;
    protected $last_name;
    protected $email;
    protected $lastlogin;

    public function __construct($date, $first_name, $last_name, $email)
    {
        $this->date = $date;
        $this->$first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
    }

    public function getFullName() {
        return $this->first_name . $this->last_name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }
}

try {
    $pdo = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = $pdo->query("SELECT * FROM users");
    $q = $query->fetchAll();
    foreach ($q as $item) {
        echo "<ul>";
        $user = new User($item['created_at'], $item['first_name'], $item['last_name'], $item['email']);
        echo "<li>" . $user->changeCreatedDate($item['created_at']) . " " .  $user->getFullName() . " " . $user->getEmail() . " " . $user->changeDate($item['created_at']) . "</li>";
        echo "</ul>";
    }
} catch (PDOException $e) {
    echo $e;
}